module polyplex.math.simplemath.compat;
import polyplex.math.simplemath;
/**
	This files provides aliases that makes it easier to transition from GLMath to SimpleMath.
**/

alias float2 = Vector2T!(float);
alias int2 = Vector2T!(int);
alias float3 = Vector3T!(float);
alias int3 = Vector3T!(int);
alias float4 = Vector4T!(float);
alias int4 = Vector4T!(int);
